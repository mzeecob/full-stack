# Task Force FullStack Challenge (Java/python)
## 

Create a full web application with backend using Java or Python and frontend using ReactJS or AngularJs.

Create, Update, Read, Delete, Search

### 🔖Requirements

**A use case:** Elie is an employer of Awesomity Lab, and as a person, he has expenses and income, 
but it is really hard for him to manage all those in and out transactions because he uses different accounts while doing all those transactions, 
some of those accounts are; bank accounts, mobile money account, cash, etc.

As a developer, Elie approaches you to develop a web application for him that has the following features;
   - Create account and SignIn.
   - track all in and out transactions from each account.
   - Generate a report according to the desired time gap.
   - Set a budget to not exceed, once exceed the application should notify him.
   - To be able to add categories and subcategories of things he spent money on.
   - To be able to link expenses with related categories or subcategories.
   - Display transactions summary in a visualized way.
   
>Your project should be on an online repository (Gitlab, Github,...)


### 📝Submission
- Submit via this Google form https://forms.gle/LhXfBemhKDQjvRmG7.
- Deadline: 31th December at noon, submit before then.

### 👷🏽‍♀️Best Practices

- Document your API eg: [Swagger](https://swagger.io/), Postman
- Apply validation (phone number must be a Rwandan number, and email should be validated).
- The system should throw an exception if any error occurs.
- Properly log your application.
- Do not hard code any sensitive data (eg: env variables).
- Write a good README file with the steps to Install, Test & run your application (steps for docker included)

### ✨Bonus

- System Logs
   - The system should record all the manager’s activities (login, logout, password reset, profile update etc…) in order to comply with external audit requirements.
- Testing
   - Integration tests for all your use cases.
   - Write unit tests with a minimum of 60% coverage.
- Have a Dockerized environment(`Dockerfile`, `docker-compose.yml`)
